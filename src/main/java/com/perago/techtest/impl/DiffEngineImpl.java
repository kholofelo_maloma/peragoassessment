package com.perago.techtest.impl;

import com.perago.techtest.Diff;
import com.perago.techtest.DiffEngine;
import com.perago.techtest.DiffException;
import com.perago.techtest.model.DataChange;
import com.perago.techtest.model.DiffStatus;

import java.lang.reflect.Field;

/**
 * @author Kholofelo Maloma
 * @purpose
 * @since 2016/11/07.
 */
public class DiffEngineImpl implements DiffEngine {

    public <T extends Object> T apply(T original, Diff<?> diff) throws DiffException {

        T modified = (T) getNewInstance(original.getClass());

        Field[] originalFields = original.getClass().getDeclaredFields();

        for (final Field originalField : originalFields) {
            try {

                originalField.setAccessible(true);
                String originalFieldName = originalField.getName();
                T diffValue = (T) diff.getFieldValue(originalFieldName);

                if (diffValue == null) {
                    diffValue = (T) originalField.get(original);
                } else {
                    if (isADependencyReference(diffValue)) {
                        modified = apply(diffValue, diff);
                    }
                }

                originalField.set(modified, diffValue);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return modified;
    }


    public <T extends Object> Diff<T> calculate(T original, T modified) throws DiffException {
        Diff<T> diff = new Diff<T>();
        diff.setDataType(original.getClass().getSimpleName());

        Field[] originalFields = original.getClass().getDeclaredFields();

        try {
            for (final Field originalField : originalFields) {
                originalField.setAccessible(true);

                final String fieldName = originalField.getName();
                final T originalValue = (T) originalField.get(original);
                final T modifiedValue = (T) originalField.get(modified);

                if (modifiedValue != null && originalValue != null) {
                    if (isADependencyReference(original)) {
                        calculate(originalValue, modifiedValue);
                    }
                }
                final DataChange dataChange = new DataChange();
                dataChange.setFieldName(fieldName);

                dataChange.setFieldValue(modifiedValue);

                if (originalValue == null && modifiedValue != null) {
                    dataChange.setDiffStatus(DiffStatus.CREATED);
                } else if (modifiedValue == null && originalValue != null) {
                    dataChange.setDiffStatus(DiffStatus.DELETED);
                } else if (!modifiedValue.equals(originalValue)) {
                    dataChange.setDiffStatus(DiffStatus.UPDATED);
                } else {
                    dataChange.setDiffStatus(null);
                }

                if (dataChange.getDiffStatus() != null) {
                    diff.addDiffValue(fieldName, dataChange);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return diff;
    }


    /**
     * This utility function tests if a given a given object belongs to a java specific object or not
     *
     * @return true if a given object is a dependant
     */
    public <T> boolean isADependencyReference(T reference) {
        String packageName = reference.getClass().getPackage().getName();
        return !packageName.startsWith("java.");
    }

    private <T extends Object> T getNewInstance(Class<T> clazz) {
        T modified = null;
        try {
            modified = clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return modified;
    }
}
