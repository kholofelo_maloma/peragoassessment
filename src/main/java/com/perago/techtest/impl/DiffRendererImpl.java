package com.perago.techtest.impl;

import com.perago.techtest.Diff;
import com.perago.techtest.DiffException;
import com.perago.techtest.DiffRenderer;
import com.perago.techtest.model.DataChange;

import java.util.Map;

/**
 * @author Kholofelo Maloma
 * @purpose
 * @since 2016/11/07.
 */
public class DiffRendererImpl implements DiffRenderer {

    public String render(Diff<?> diff) throws DiffException {
        String renderBody = null;
        if(diff == null){
            return null;
        }
        /**
         * Notes : Used String <code>+</code> to concutinate the strings instead of StringBuilder.append() because since Java 1.6, the compiler automatically
         * changed <code>+</code> operator to <code>StringBuilder#append()</code>
         */
        renderBody = diff.getDataType() +"\n\n";

        for (Map.Entry<String, DataChange> changeEntry : diff.getDataChangeByFieldName().entrySet()) {
            renderBody += "\t|"+changeEntry.getKey() + " | " + changeEntry.getValue().getFieldValue() + " | " + changeEntry.getValue().getDiffStatus().getStatusName() + " |\n";
        }

        return renderBody;
    }
}
