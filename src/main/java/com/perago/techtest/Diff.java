package com.perago.techtest;

import com.perago.techtest.model.DataChange;
import com.perago.techtest.model.DiffStatus;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * The object representing a diff.
 * Implement this class as you see fit.
 */
public class Diff<T extends Object> {

    private String dataType;
    private Map<String, DataChange> dataChangeByFieldName = new HashMap<String, DataChange>();

    public T getFieldValue(String originalFieldName) {
        DataChange dataChange = dataChangeByFieldName.get(originalFieldName);
        if (dataChange != null) {
            return (T) dataChange.getFieldValue();
        }
        return null;
    }

    public DataChange getDataChangeForFieldName(final String fieldName) {
        return dataChangeByFieldName.get(fieldName);
    }

    public <V extends Object> void putDiffValue(final String fieldName, final V fieldValue) {
        DataChange<V> dataChange;
        if (dataChangeByFieldName.get(fieldName) != null) {
            dataChange = dataChangeByFieldName.get(fieldName);
            dataChange.setDiffStatus(DiffStatus.UPDATED);
            dataChange.setFieldValue(fieldValue);
        } else if (dataChangeByFieldName.get(fieldName) == null) {
            dataChange = new DataChange<V>();
            dataChange.setDiffStatus(DiffStatus.CREATED);
            dataChange.setFieldValue(fieldValue);
            dataChange.setFieldName(fieldName);
            dataChangeByFieldName.put(fieldName, dataChange);
        }
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(final String dataType) {
        this.dataType = dataType;
    }

    public Map<String, DataChange> getDataChangeByFieldName() {
        return dataChangeByFieldName;
    }

    @Override
    public String toString() {
        /**
         * I could have used the plus operator(+) to join the strings simply because since Java 1.6, the java compiler would replace the + with a similar implementation
         * as the one below, however, it does not always do it so optimal. So yah,  just gonna do it manually
         */
        final StringBuilder stringBuilder = new StringBuilder("Diff {");

        for (Map.Entry<String, DataChange> stringDataChangeEntry : dataChangeByFieldName.entrySet()) {
            stringBuilder.append(stringDataChangeEntry.getKey()).append("=[ ").append(stringDataChangeEntry.getValue()).append("]\n");
        }
        stringBuilder.append(" }");

        return stringBuilder.toString();
    }

    public void addDiffValue(String fieldName, DataChange dataChange) {
        dataChangeByFieldName.put(fieldName, dataChange);
    }
}
