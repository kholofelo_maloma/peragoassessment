package com.perago.techtest.model;

/**
 * @author Kholofelo Maloma
 * @purpose
 * @since 2016/11/07.
 */
public enum DiffStatus{
    CREATED("Created"),
    UPDATED("Updated"),
    DELETED("Deleted");

    private final String statusName;

     DiffStatus(final String statusName){
        this.statusName = statusName;
    }

    public String getStatusName() {
        return statusName;
    }

    @Override
    public String toString() {
        return "DiffStatus{" +
                "statusName='" + statusName + '\'' +
                '}';
    }
}
