package com.perago.techtest.model;

/**
 * @author Kholofelo Maloma
 * @purpose
 * @since 2016/11/07.
 */
public class DataChange<V extends Object> {

    private String fieldName;
    private V fieldValue;
    private DiffStatus diffStatus;


    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(final String fieldName) {
        this.fieldName = fieldName;
    }

    public V getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(final V fieldValue) {
        this.fieldValue = fieldValue;
    }

    public DiffStatus getDiffStatus() {
        return diffStatus;
    }

    public void setDiffStatus(final DiffStatus diffStatus) {
        this.diffStatus = diffStatus;
    }

    @Override
    public String toString() {
        return "DataChange{" +
                "fieldName='" + fieldName + '\'' +
                ", fieldValue=" + fieldValue +
                ", diffStatus=" + diffStatus +
                '}';
    }
}
