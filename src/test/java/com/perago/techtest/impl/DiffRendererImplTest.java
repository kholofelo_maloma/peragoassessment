package com.perago.techtest.impl;

import com.perago.techtest.Diff;
import com.perago.techtest.DiffRenderer;
import com.perago.techtest.test.Person;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static com.perago.techtest.TestUtility.findGoodFriend;

/**
 * @author Kholofelo Maloma
 * @purpose
 * @since 2016/11/07.
 */
public class DiffRendererImplTest {

    private DiffRenderer diffRenderer;

    @Before
    public void setUp() {
        diffRenderer = new DiffRendererImpl();
    }

    @Test
    public void render() throws Exception {

        /**
         * I could use assert checks, but this is a visual display test, so I  will just use standard output
         */

        Person originalPerson = new Person();
        originalPerson.setFirstName("Kholofelo"); //To be Updated
        originalPerson.setSurname("Maloma"); // to be deleted
        originalPerson.setFriend(null);// to be created

        Set<String> nickNames = new HashSet<String>();
        nickNames.add("Kholo");
        originalPerson.setNickNames(nickNames);// to be left unchanged

        Person modifiedPerson = new Person();
        modifiedPerson.setFirstName("Someone Else");
        modifiedPerson.setSurname(null);
        modifiedPerson.setFriend(findGoodFriend());
        modifiedPerson.setNickNames(nickNames);

        originalPerson.setFriend(modifiedPerson);

        Diff diff = new DiffEngineImpl().calculate(originalPerson, modifiedPerson);

        System.out.println(diffRenderer.render(diff));

    }

}