package com.perago.techtest.impl;

import com.perago.techtest.Diff;
import com.perago.techtest.DiffEngine;
import com.perago.techtest.TestUtility;
import com.perago.techtest.model.DiffStatus;
import com.perago.techtest.test.Person;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * @author Kholofelo Maloma
 * @purpose
 * @since 2016/11/07.
 */
public class DiffEngineImplTest {

    private DiffEngineImpl diffEngine;

    @Before
    public void setUp() throws Exception {
        diffEngine = new DiffEngineImpl();
    }

    @Test
    public void apply() throws Exception {
        Person person = new Person();
        person.setFirstName("Kholofelo");
        person.setSurname("Maloma");

        Diff<Person> personDiff = new Diff<Person>();
        personDiff.putDiffValue("firstName", "Changed Kholofelo");

        Person differentPerson = diffEngine.apply(person, personDiff);

        assertEquals("field comparison failed at firstName", "Changed Kholofelo", differentPerson.getFirstName());
        assertEquals("field comparison failed at surname", "Maloma", differentPerson.getSurname());
        assertNull("field comparison failed at field", differentPerson.getFriend());

    }

    @Test
    public void calculate() throws Exception {

        Person originalPerson = new Person();
        originalPerson.setFirstName("Kholofelo"); //To be Updated
        originalPerson.setSurname("Maloma"); // to be deleted
        originalPerson.setFriend(null);// to be created

        Set<String> nickNames = new HashSet<String>();
        nickNames.add("Kholo");
        originalPerson.setNickNames(nickNames);// to be left unchanged

        Person modifiedPerson = new Person();
        modifiedPerson.setFirstName("Someone Else");
        modifiedPerson.setSurname(null);
        modifiedPerson.setFriend(TestUtility.findGoodFriend());
        modifiedPerson.setNickNames(nickNames);

        Diff diff = diffEngine.calculate(originalPerson, modifiedPerson);

        assertNotNull(diff);
        assertNotNull(diff.getDataChangeForFieldName("firstName"));
        assertNotNull(diff.getDataChangeForFieldName("surname"));
        assertNotNull(diff.getDataChangeForFieldName("friend"));

        assertEquals("Someone Else", diff.getDataChangeForFieldName("firstName").getFieldValue());
        assertEquals(DiffStatus.UPDATED, diff.getDataChangeForFieldName("firstName").getDiffStatus());

        assertNull(diff.getDataChangeForFieldName("surname").getFieldValue());
        assertEquals(DiffStatus.DELETED, diff.getDataChangeForFieldName("surname").getDiffStatus());

        assertNotNull(diff.getDataChangeForFieldName("friend").getFieldValue());
        assertEquals(DiffStatus.CREATED, diff.getDataChangeForFieldName("friend").getDiffStatus());
    }

    @Test
    public void isADependencyReferenceWhenItIsNOTJavaSpecific() {
        assertTrue(diffEngine.isADependencyReference(new Person()));
    }
    @Test
    public void isADependencyReferenceWhenItIsJavaSpecific() {
        assertFalse(diffEngine.isADependencyReference(new Integer("-1")));
    }
}
