package com.perago.techtest;

import com.perago.techtest.test.Person;

/**
 * @author Kholofelo Maloma
 * @purpose
 * @since 2016/11/07.
 */
public class TestUtility {

    public static Person findGoodFriend() {
        final Person friend = new Person();
        friend.setFirstName("Nice One");
        friend.setSurname("Programmer");
        return friend;
    }

}
